# Opendoors (Name pending)

## What is it?

Opendoors is a tower defense game inspired by the WarcraftIII tower defense played
at LUG lan parties. If you're familiar with tower defense as a genre, then you
will already grasp the main point of this game. If not, I would recommend
looking up "tower defense" online as there are tons of examples. The rest of
this section will describe how it is different.

### Doors

As the name would imply, the biggest departure from traditional tower defense
comes from having to open doors. The game begins in a single small area that
fits in a single screen. This initial room has 4 doors on each cardinal
direction. Enemies enter the game from those doors. On a regular interval,
players will be required to open one of the closed doors around them. Doing so
will reveal a new room with new closed doors. The newly revealed closed doors
spawn enemies as well. As the game progresses, players open more and more doors
which brings about more and more enemies.

Each player votes on which door to open. Voting requires being physically near a
door. If not all players vote, there's a chance a random door will open.

### Rooms

Each room has its own layout which impacts where towers can be placed and where
enemies will travel. Some rooms might help the players' situation while others
might harm it.

### Towers

Placing a tower in a room helps you defeat enemies. Different towers will have
different enemies they're best at defeating. Towers cost money which is dropped
by enemies. Towers can be upgraded and downgraded. Some downgrades might
preserve certain attributes of the upgraded tower.

Based on how doors work, it might make sense to build a square area when you
open doors to minimize the number of closed doors in the system. Doing so though
will result in having less money which means less towers/upgrades as more
challenging spawns occur. On the other hand, having too many closed doors can
easily overwhelm you. A balance is therefore required.

### Classes

Each player has a class which determines how they fight. Players can directly
attack enemies to assist their towers when things get rough. Like towers, a
player's class can be upgraded or downgraded.

### Winning

Although winning is not currently implemented, the current idea follows. Some
distance d from the starting room, there will be an x% chance that the door
opens to reveal the final boss/wave. If players complete that, they win the
game. It's fairly simple, but opens a level of uncertainty around when the game
will end. Similar to the tower defense played at LUG, it should take ~40 minutes
to complete the game.

### Multiplayer

The game should be fun at lan/wan parties. Multiple players should be
able to divvy up the tower management to protect the center. The difficulty will
hopefully scale in a fun way as more players are added. The inspiration for this
is the 13 player WarcraftIII tower defense mod.


## What's the current state?

A lot of groundwork has been created for the game. Enemies spawn, towers can be
built/upgraded, and classes work. What work we do next and what architectural
decisions wee still need to make depend largely on what the actual content of
the game will look like. The current content will need to be modified/scrapped
to make room for balanced, fun content. The game is far enough along such that
you can create new content in one sitting although not all content will be that
easy. There is also working but likely buggy multiplayer that needs to be
improved as we find problems.

## How can I help?

You should first join the developer chat at
https://app.element.io/#/room/!fjdfHgsqcOwnWNwGKc:matrix.org. You can check out
previous discussions, voice your thoughts, and get help creating stuff there.
As of early November, there's not a lot of specific work to do but we will be
adding Gitlab issues to this project as we settle on what needs to be created.

We will need a combination of code work and asset creation. You should check out
the documentation for the game engine Godot to understand how it works.
Development will likely be reasonably split between writing GDScript (a language
very similar to Python) and creating assets in the editor/in other tools.

## Deadlines:

These deadlines exist to make sure the project doesn't get stuck in development.
If any of them aren't met, it will likely spell doom for the project.

1. Have a "playable" demo available for the next LAN party this semester. Although
people might not want to play it, it should exist. This is at most a month away.


2. Have a 1.0 version of the game available by the first LAN part next semester.
   Not only should the game be playable, but people should want to play a second
   round after completing the first one.

3. ??? To be filled in once we hit the first two deadlines.
