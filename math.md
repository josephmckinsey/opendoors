Brain dump of balance ideas where I just wrote down equations and hoped to find
something useful when I did. It might be incorrect.

rn = number of rooms
dn = number of doors

A game with `rn` rooms will have between `sqrt(rn)*4` doors when arranged as a
square and `rn*2+2` doors when arranged as a line.

To defeat an enemy that exists for `t` time and `h` health, you will need to hit
it with `h / t = dps` along its route.

Regardless of distance, an enemy type should spend the same amount of
time in the game. This means speed grows linearly with distance.

If there are `en` enemies, your `dps` needs to be `dps*en` to survive. The
number of enemies `en` is equal to `dn * rate`. Expanding out those variables
gives you `h/t*dn*rate`. The variable `t` will be a constant (unless you use a
glue trap), `h` will do something (currently it increases linearly with
distance), `rate` will increase with time using a step function, and `dn` will
increase but the rate of increase is controlled by the players.

If you defeat `dn * rate` enemies every second and each enemy is worth $1, then
you will get `dn*rate` income per second. If you're trying to keep up with the
`h/t=dps` rate, then you'll need to upgrade/build more towers. Assuming `h`
remains constant, you'll need to build towers in every new room that opens to
keep the `dps` constant.

Assume you have `tn` towers across a distance of `d` to a door achieving `dps`
of damage per second. Each tower costs `tc`. Up to this point, you've paid `tn *
tc` for the towers. When the door opens, you will have another `2` distance to
cover with towers. The cost to account for the new room will be `tn * tc / d *
2`. This assumes that you only reveal one additional door. If you unveil 2 or 3
additional doors, then you'll have to double or triple your `dps`. You'll also
double or triple your income along that route. Duplicating every tower to make
up the `dps` is the worst case option in terms of price. Upgrading should give
you an increase in `dps` for some fraction of duplicating every tower.

That means a tower costing `tc` should cost `tc / n` to upgrade where `n` is
larger than 1.
