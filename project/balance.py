import yaml
import pathlib
from collections import deque
from math import sqrt, ceil, floor
from copy import deepcopy

# This file calculates various stats and prints them out. Unfortanetly, my
# mathematical modeling skills quickly fall flat when trying to do anything
# nontrivial. I'm totally willing to change the game mechanics and the formulas
# here to make things work.

# This script uses the existing yaml files so you won't need to do any extra
# copying to test a game's balance.
with open("towers.yaml") as f:
    towers = yaml.safe_load(f.read())


# The rounds array holds the raw data while the stats array will get filled with
# summaries about the rounds.
rounds = []
roundStats = []
for i in range(len(list(pathlib.Path('rounds').iterdir()))):
    with open(f"rounds/{i+1}.yaml") as f:
        rounds.append(yaml.safe_load(f.read()))
        roundStats.append({})


# What kind of prize/reward do enemies give? Ghosts give you $1.
enemyPrize = {"Ghost": 1}

# This function calculates a score for a round based on a metric for measuring
# enemies and functions that calculate the lower and upper bounds based on room
# orientation. The dynamic room layouts is a real pain to model.
def roundScore(metric, lowerCalc, upperCalc):
    allRoundMoney = []
    roundNum = 0
    for round in rounds:
        roundNum += 1
        spawnTimes = [(59.9, 0)]
        roundMoney = 0
        for action in round:
            # This calculation doesn't account for the fact that 60 enemies
            # across 60 seconds might be different from 60 enemies in 1 second.
            # It doesn't matter for money but does seem to matter when
            # calculating health stats. I'm not really sure how to account for
            # that.
            if action['event'] == "spawn":
                spawnTimes.append((action['time'], action['rate'], metric[action['enemy']]))
        for i in range(len(spawnTimes) - 1):
            start, rate, value = spawnTimes.pop()
            roundMoney += ((spawnTimes[-1][0] - start) // rate + 1) * value
        allRoundMoney.append((roundMoney * lowerCalc(roundNum), roundMoney * upperCalc(roundNum)))
    return allRoundMoney

# The minimum number of doors happens when you arrange them like a square. This
# function calculates the number of doors assuming you're building towards a
# square.
def minDoors(n):
    edgeSide = ceil(sqrt(n))
    numRows = ceil(n / edgeSide)
    complete = numRows * 2 + edgeSide * 2
    return complete
    

# Arranging everything as a line (the maximum number of doors) is easy to
# calculate so it's just a lambda.
for i, score in enumerate(roundScore(enemyPrize, minDoors, lambda x: x * 2 + 2)):
    roundStats[i]["money"] = score


# Health is a little weird. The amount of health an enemy has is directly
# proportional to its starting position's distance from the center room. For
# example, if ghosts spawn in a room at location (5, 0) then the ghosts will
# have 5x as much health. I'm totally willing to change this machanic.
def squareHealth(n):
    a = set()
    for i in range(n):
        a.add((i // ceil(sqrt(n)) + n - sqrt(n)//2, i % ceil(sqrt(n)) + n - sqrt(n)//2))

    s = 0
    for x, y in a:
        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                if abs(dx)+abs(dy) == 1 and (x+dx, y+dy) not in a:
                    s += sqrt((x-n)**2 + (y-n)**2) + 1
    return s


print(roundStats)
health = {"Ghost": 3}
# Yay for lines still being easy to calculate (at least I think lines are the max).
# for i, score in enumerate(roundScore(health, squareHealth, lambda n: n*(n+2)+1)):
#     roundStats[i]["health"] = score
# print(roundStats)

# This calculation seems to underestimate the number of towers needed and it
# assumes towers have infinite range. If ghosts always take 10 seconds to reach
# the middle regardless of starting location and towers don't have infinite
# range, then more towers will be needed to maintain a steady DPS. As for why it
# always underestimates things, I have no idea. It seems like the towers
# sometimes pick suboptimal targets? How do I quantify this?
# print("How many arrow towers are needed to keep up with the damage?")
# arrowDefault = towers["Arrow"]["default"]
# dpmArrow = arrowDefault["damage"] / arrowDefault["rate"] * 60
# for r in roundStats:
#     minH, maxH = r["health"]
#     print((minH / dpmArrow, maxH / dpmArrow))
