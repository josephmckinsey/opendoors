# How to add a Class

1. Create a new folder and main tscn and gd file for the class. See the other
   classes for an example.

2. Add your class to the MainMenu.tscn option button.

3. Add your class to the two parts mentioned in the menu.gd file.

That should be it! If it's not, let me (jhgarner) know so I can update the
README or fix the code.
