extends Node

# See ../ranged/Ranged.gd for mode docs

const FIST = preload("res://classes/melee/Fist.tscn")
const THIS = "res://classes/melee/Melee.tscn"
onready var root = get_node("/root/MainLoc").main

var cooldown = 0
var cooldownTime = 0.5
var instances = []
var cost = 1
var i = 0

func setup(params):
	cooldownTime = params[0]
	name = "Melee" + str(params[1])
	i = params[1] + 1

remotesync func spawn():
	var fist = FIST.instance()
	fist.position = Vector2(0, 0)
	add_child(fist)

func getUpgrades():
	instances = [[THIS, "Faster", 1, [cooldownTime * 0.5, i]]]
	return instances

func getDowngrades():
	return null

func _process(delta):
	if Input.is_action_pressed("attack") and cooldown >= cooldownTime:
		rpc("spawn")
		cooldown -= cooldownTime
	else:
		cooldown += delta
		cooldown = min(cooldown, cooldownTime)
