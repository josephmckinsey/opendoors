extends Node


# This code is nearly identical to Melee.gd.

const ARROW = preload("res://classes/ranged/Projectile.tscn")
const THIS = "res://classes/ranged/Ranged.tscn"

onready var root = get_node("/root/MainLoc").main

var cost = 1
# The amount of time we've the attack has been cooling down for
var cooldown = 0
# The amount of time we have to cool down for
var cooldownTime = 0.5
# The number of times we've updated
var i = 0

# Called right after node creation and before getting assigned a parent
func setup(params):
	cooldownTime = params[0]
	name = "Ranged" + str(params[1])
	i = params[1] + 1

# Called on all players to spawn a projectile (AKA arrow for now)
remotesync func spawn():
	var fist = ARROW.instance()
	fist.position = Vector2(0, 0)
	add_child(fist)

# The only upgrade is making the shooting faster
# [ Path of resource to replace this one with
# , Name of upgrade as displayed to user
# , cost of node
# , object passed to setup
# ]
func getUpgrades():
	return [[THIS, "Faster", 1, [cooldownTime * 0.9, i]]]

# Would have the same structure as upgrades
# TODO should upgrades and downgrades be the same?
func getDowngrades():
	return null

func _process(delta):
	if Input.is_action_pressed("attack") and cooldown >= cooldownTime:
		rpc("spawn")
		cooldown -= cooldownTime
	else:
		cooldown += delta
		cooldown = min(cooldown, cooldownTime)
