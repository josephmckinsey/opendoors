extends Area2D

const AStar2DR = preload("res://scripts/AStar2DR.gd")
const enemies = preload("res://EnemyResource.tres")

onready var root = get_node("/root/MainLoc").main

var progress = 0

var enemyType = ""

remote var path: Array
remote var speed
remote var health
remote var fullHealth
remote var money


func _ready():
	if root.isServer:
		var myType = enemies.contents[enemyType]
		$Sprite.texture = myType.sprite
		var pathing = root.pathing
		var start = pathing.get_closest_point2d(position)
		var end = pathing.get_closest_point2d(Vector2(0, 0))
		money = myType.money
		# path = pathing.get_path2d(start, end, 4)
		# path.push_back(position)
		update_path(0, 0)
		if abs(position.x) < 500 and abs(position.y) < 500:
			fullHealth = myType.health
			speed = myType.speed
		else:
			fullHealth = myType.health + myType.health * sqrt(pow(round((abs(position.x)) / 1000), 2) + pow(round((abs(position.y)) / 1000), 2))
			speed = myType.speed + myType.speed * sqrt(pow(round((abs(position.x)) / 1000), 2) + pow(round((abs(position.y)) / 1000), 2))
		health = fullHealth
		rset("path", path)
		rset("speed", speed)
		rset("fullHealth", fullHealth)
		rset("health", health)
		rset("money", money)

func wander(delta):
	if progress >= 1:
		path.pop_back()
		progress = 0
	if len(path) > 1:
		var length = sqrt((path[-2] - path[-1]).dot(path[-2] - path[-1]))
		if (abs(length) < 0.000001):
			path.pop_back()
			progress = 0
			return
		position = (path[-2] - path[-1]) * progress + path[-1]
		# speed = d/t,
		progress += (speed*delta) / length
	else:
		var x = [][0]
		# update_path(-1000, 1000)

func update_path(a, b):	
	if root.isServer:
		var pathing = root.pathing
		var start = pathing.get_closest_point2d(position)
		var end = pathing.get_closest_point2d(Vector2(rand_range(a, b), rand_range(a, b)))
		path = pathing.get_path2d(start, end, 4)
		path.insert(0, position)
		rset("path", path)

func damage(amount):
	health -= amount
	get_node("Sprite").modulate = Color.white.darkened(1 - (float(health) / fullHealth))

func onGoodCollision(body):
	if body.is_in_group("good"):
		body.damage(2)
		damage(1000000)

func _physics_process(delta):
	if health <= 0:
		root.money += money
		queue_free()
	wander(delta)
