extends Resource

var contents

var yaml = preload("res://addons/godot-yaml/gdyaml.gdns").new()

func _init():
	var file = File.new()
	file.open("res://enemies.yaml", File.READ)
	file = file.get_as_text()
	contents = yaml.parse(file)
	for enemy in contents:
		if contents[enemy].has('sprite'):
			contents[enemy].sprite = load("res://" + contents[enemy].sprite)
