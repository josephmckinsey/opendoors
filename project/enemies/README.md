# How to add a new enemy

1. Create a new folder, gd, and tscn file for your enemy. Consider copying the
   one for ghost.

2. Add your enemy to the array near the top of the RoomLayout file.

3. Update the timer.dat file with a new column for your enemy.

That should be it! If it's not, let me (jhgarner) know so I can update the
README or fix the code.
