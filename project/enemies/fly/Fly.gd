extends Area2D

# The fly acts like the ghost but doesn't damage the trophy. Instead, it flies
# around the trophy while distracting your towers.


# Flies are a little broken in that their path is kind of flipped, but it also
# opens an interesting idea that some enemies could skip over certain
# obstacles. If you want to fix it though, either reverse the path before
# storing it or switch the indices to operate on the other side.

export (int) var speed = 1000
export (int) var accel = 10

const AStar2DR = preload("res://scripts/AStar2DR.gd")

var progress = 0
var fullHealth = 1
var health = fullHealth

remote var path: Array

func _ready():
	update_path(0, 0)
	
func update_path(a, b):	
	if get_node("/root/Main").isServer:
		var pathing = get_node("/root/Main").pathing
		var start = pathing.get_closest_point2d(position)
		var end = pathing.get_closest_point2d(Vector2(rand_range(a, b), rand_range(a, b)))
		path = pathing.get_path2d(start, end, 100)
		path.insert(0, position)
		rset("path", path)

func wander(delta):
	if progress >= 1:
		path.pop_back()
		progress = 0
	if len(path) > 1:
		var length = sqrt((path[-2] - path[-1]).dot(path[-2] - path[-1]))
		if (abs(length) < 0.000001):
			path.pop_back()
			progress = 0
			return
		position = (path[-2] - path[-1]) * progress + path[-1]
		# speed = d/t,
		progress += (speed*delta) / length
	else:
		update_path(-1000, 1000)

func damage(amount):
	health -= amount
	get_node("Sprite").modulate = Color.white.darkened(1 - (float(health) / fullHealth))

func _physics_process(delta):
	if health <= 0:
		get_node("/root/Main").money += 1
		queue_free()
	wander(delta)




func onGoodCollision(body):
	# pass
	if body.is_in_group("good"):
		update_path(-1000, 1000)
