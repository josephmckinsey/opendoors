extends Area2D

remote var speed = 50
var speedMod = 1

const AStar2DR = preload("res://scripts/AStar2DR.gd")

var progress = 0
remote var fullHealth = 3
remote var health = fullHealth

remote var path: Array

func _ready():
	if get_node("/root/Main").isServer:
		var pathing = get_node("/root/Main").pathing
		var start = pathing.get_closest_point2d(position)
		var end = pathing.get_closest_point2d(Vector2(0, 0))
		path = pathing.get_path2d(start, end, 4)
		path.push_back(position)
		if abs(position.x) < 500 and abs(position.y) < 500:
			fullHealth = 3
			speed = 40
		else:
			fullHealth = 3 + 3 * sqrt(pow(round((abs(position.x)) / 1000), 2) + pow(round((abs(position.y)) / 1000), 2))
			speed = 40 + 40 * sqrt(pow(round((abs(position.x)) / 1000), 2) + pow(round((abs(position.y)) / 1000), 2))
		health = fullHealth
		rset("path", path)
		rset("speed", speed)
		rset("fullHealth", fullHealth)
		rset("health", health)
	

func wander(delta):
	if progress >= 1:
		path.pop_back()
		progress = 0
	if len(path) > 1:
		var length = sqrt((path[-2] - path[-1]).dot(path[-2] - path[-1]))
		if (abs(length) < 0.000001):
			path.pop_back()
			progress = 0
			return
		position = (path[-2] - path[-1]) * progress + path[-1]
		# speed = d/t,
		progress += (speed*speedMod*delta) / length

func damage(amount):
	health -= amount
	get_node("Sprite").modulate = Color.white.darkened(1 - (float(health) / fullHealth))

# Currently unused, but might be useful if we want to sync the ghosts from the
# host to the other players.
func updateState(pos, newPath, newHealth, prog):
	progress = prog
	position = pos
	path = newPath
	health = newHealth
	

func _physics_process(delta):
	if health <= 0:
		get_node("/root/Main").money += 1
		queue_free()
	wander(delta)



# Attacks the trophy
func onGoodCollision(body):
	if body.is_in_group("good"):
		body.damage(2)
		queue_free()
