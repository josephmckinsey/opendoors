tool
extends EditorScript

const GRID = preload("res://scenes/GridCell.tscn")

func _run():
	var parent = get_scene()

	for row in range(0, 10):
		for col in range(0, 10):
			var grid = GRID.instance()
			# grid.init(col, row)
			parent.add_child(grid)
			grid.set_owner(get_scene())
			grid.set_position(Vector2((col-5) * 96, (row-5) * 96))

