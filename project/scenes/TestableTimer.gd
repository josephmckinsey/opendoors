extends Node2D

signal timeout

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var wait_time = 100
var time = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	time += delta
	if time >= wait_time:
		emit_signal("timeout")
		time -= wait_time

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
