extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var messages = get_node("VBoxContainer/Control/VBoxContainer/Messages")
onready var messageUI = get_node("VBoxContainer/Control/VBoxContainer")
onready var notification = get_node("VBoxContainer/Control/Notification")
onready var style = get_stylebox("panel", "")
onready var edit = get_node("VBoxContainer/HBoxContainer/TextEdit")
onready var root = get_node("/root/MainLoc").main
var isFocused = false

# Called when the node enters the scene tree for the first time.
func _ready():
	messageUI.modulate = Color(0, 0, 0, 0)

func add_message(m):
	m = m.replace("\\n", "\n")
	if !isFocused:
		notification.modulate = Color(1, 1, 1, 1)
		$HideTimer.start()
	style.bg_color = Color(0, 0, 0, 152.0/255)
	messages.text += "\n> " + m
	notification.text = m

func _hide():
	notification.modulate = Color(0, 0, 0, 0)
	if !isFocused:
		style.bg_color = Color(0, 0, 0, 0)

func _no_focus():
	isFocused = false
	root.dontMove = false
	style.bg_color = Color(0, 0, 0, 0)
	messageUI.modulate = Color(0, 0, 0, 0)

func _focus():
	isFocused = true
	root.dontMove = true
	notification.modulate = Color(0, 0, 0, 0)
	messageUI.modulate = Color(1, 1, 1, 1)
	style.bg_color = Color(0, 0, 0, 152.0/255)

func _process(_delta):
	if Input.is_action_pressed("open_text"):
		edit.grab_focus()
	if Input.is_action_pressed("ui_cancel"):
		edit.release_focus()
		_no_focus()

func _mouse_out():
	edit.release_focus()

func _send_arg(_unused):
	_send()
func _send():
	var m = $VBoxContainer/HBoxContainer/TextEdit.text
	if m == "start":
		root.start()
	if m == "pause":
		root.pause()
	if m.begins_with("money "):
		root.money = int(m.split(" ")[1])
	if m == "skip":
		root.on_door_timer(true)
	if m.begins_with("save "):
		print("SAVED")
		root.serialize(m.split(" ")[1])
	if m.begins_with("load "):
		print("LOADED")
		root.load(m.split(" ")[1])
	add_message(m)
	$VBoxContainer/HBoxContainer/TextEdit.text = ""
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
