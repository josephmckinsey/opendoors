tool

extends ColorRect

func _process(_delta):
	if Engine.editor_hint:
		if get_parent().canHoldTower:
			color = Color.aqua
			color.a = 0.3
		else:
			color = Color.green
			color.a = 0.3
	else:
		color = Color.transparent
