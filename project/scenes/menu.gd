extends Control

export var startButtonPath: NodePath

const Main = preload("res://scenes/Main.tscn")
const PUPPET = preload("res://scenes/PuppetPlayer.tscn")
const MELEE = preload("res://classes/melee/Melee.tscn")
const RANGED = preload("res://classes/ranged/Ranged.tscn")
const MainLoc = preload("res://scenes/MainLoc.tscn")

const SERVER_PORT = 5000
const MAX_PLAYERS = 4

onready var root = get_node("/root")
onready var startButton = get_node(startButtonPath)

var main
var myid

# # Player info, associate ID to data
var player_info = {}
# # Info we send to other players
var my_info = {classChoice = ""}

func _player_connected(id):
	# Called on both clients and server when a peer connects. Send my info to it.
	rpc_id(id, "register_player", my_info)

func _player_disconnected(id):
	player_info.erase(id) # Erase player from info.

func _connected_ok():
	pass # Only called on clients, not server. Will go unused; not useful here.

func _server_disconnected():
	pass # Server kicked us; show error and abort.

func _connected_fail():
	pass # Could not even connect to server; abort.

remote func register_player(info):
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	# Store the info
	player_info[id] = info

func _ready():
	get_tree().paused = true

	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

func _start():
	rpc("start")

remotesync func start():
	root.get_child(0).queue_free()
	make_main()
	if myid == 1:
		main.isServer = true
		main.setup()
	main.numPlayers = player_info.size() + 1
	main.get_node("Player").name = "Player" + str(myid)
	main.myid = str(myid)
	for player in player_info:
		var pup = PUPPET.instance()
		pup.name = "Player" + str(player)
		# Add class here!
		if player_info[player]["classChoice"] == "Melee":
			pup.get_node("Sprite").add_child(MELEE.instance())
		if player_info[player]["classChoice"] == "Ranged":
			pup.get_node("Sprite").add_child(RANGED.instance())
		pup.get_node("Sprite").get_node("Attack").set_process(false)
		main.add_child(pup)
	get_tree().paused = false

func make_main():
	var class_choice = get_node("CenterContainer/VBoxContainer/OptionButton").text
	main = Main.instance()
	var main_loc = MainLoc.instance()
	main_loc.main = main
	main_loc.name = "MainLoc"
	# if get_node("/root/MainLoc"):
	# 	pass
	# else:
	get_node("/root/").add_child(main_loc)
	# Add class here!
	if class_choice == "Melee":
		main.get_node("Player/Sprite").add_child(MELEE.instance())
	if class_choice == "Ranged":
		main.get_node("Player/Sprite").add_child(RANGED.instance())
	root.add_child_below_node(self, main)
	main.set_network_master(1)
	
func _host():
	my_info["classChoice"] = get_node("CenterContainer/VBoxContainer/OptionButton").text
	startButton.disabled = false

	# Setup networking
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(SERVER_PORT, MAX_PLAYERS)
	get_tree().network_peer = peer
	myid = 1

	

func _guest():
	my_info["classChoice"] = get_node("CenterContainer/VBoxContainer/OptionButton").text
	# Setup networking
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client("localhost", SERVER_PORT)
	get_tree().network_peer = peer
	myid = get_tree().get_network_unique_id()
