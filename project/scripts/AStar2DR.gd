extends AStar

class_name AStar2DR

var randomOffset = 0

func _compute_cost(u, v):
	return abs(u - v) + rand_range(-randomOffset, randomOffset)

func add_point2d(id, v2):
	add_point(id, Vector3(v2.x, v2.y, 0))

func get_closest_point2d(v2):
	return get_closest_point(Vector3(v2.x, v2.y, 0))

func get_path2d(a, b, random):
	randomOffset = random
	var v3 = get_point_path(a, b)
	var v2s = PoolVector2Array()
	v2s.resize(v3.size())
	
	for i in range(0, v3.size()):
		v2s[i] = Vector2(v3[i].x, v3[i].y)
	v2s.invert()
	return v2s

func _estimate_cost(u, v):
	return min(0, abs(u - v) - 1)
