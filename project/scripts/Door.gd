extends StaticBody2D

class_name Door

enum Direction {
	UP, DOWN, LEFT, RIGHT
}


var x = 0
var y = 0
var parent
export (Direction) var direction: int = Direction.UP # Direction ENUM

var until_spawn = {}
var last_rate = {}

onready var root = get_node("/root/MainLoc").main


func _ready():
	parent = get_parent()
	for i in root.current_rates:
		until_spawn[i] = 0
		last_rate[i] = 0

# Only called on the host
func spawn():
	for enemyType in root.current_rates:
		var rate = root.current_rates[enemyType]
		if last_rate[enemyType] != rate:
			last_rate[enemyType] = rate
			until_spawn[enemyType] = 0
		elif rate == 0:
			until_spawn[enemyType] = 0
		elif until_spawn[enemyType] <= 0:
			for _j in range(root.current_burst[enemyType]):
				var r = rand_range(3, 6)
				var pos = 0
				until_spawn[enemyType] += rate
				if direction == Direction.LEFT:
					pos = parent.gridToMyLocal(0, r)
				elif direction == Direction.RIGHT:
					pos = parent.gridToMyLocal(9, r)
				elif direction == Direction.UP:
					pos = parent.gridToMyLocal(r, 0)
				elif direction == Direction.DOWN:
					pos = parent.gridToMyLocal(r, 9)
				pos += parent.position
				root.rpc('create_ghost', enemyType, pos, root.ghostNum)
				root.create_ghost(enemyType, pos, root.ghostNum)
				root.ghostNum += 1


# Technically run on all players, but only the host gets in the for loop.
func _process(delta):
	if not root.isPaused:
		for i in until_spawn:
			until_spawn[i] -= delta
	spawn()
