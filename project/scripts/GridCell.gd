extends Control

class_name GridCell

var tower = null
var isCreated = false
var baseColor
var holdsPlayer = false
onready var root = get_node("/root/MainLoc").main

export var canHoldTower = false


# Don't place a tower on the player
func onPlayerEntered(_body):
	holdsPlayer = true
	if tower != null:
		tower.modulate = Color(1, 0.5, 0.5, 0.3)
func onPlayerLeft(_body):
	holdsPlayer = false
	if tower != null:
		tower.modulate = Color(1, 1, 1, 0.3)

# Display a ghost of the tower
func onMouseEntered():
	if !canHoldTower or isCreated:
		return
	tower = root.active_tower.instance()
	tower.get_node("StaticBody2D").layers = 0
	if holdsPlayer:
		tower.modulate = Color(1, 0.5, 0.5, 0.3)
	else:
		tower.modulate = Color(1, 1, 1, 0.3)
	tower.position = Vector2(48, 48)
	add_child(tower)

func onMouseExited():
	if !canHoldTower:
		return
	if !isCreated and tower != null:
		tower.queue_free()
		tower = null

# Start building the tower on just this instance of the game.
func onMouseClick(event):
	var money = root.money
	if !canHoldTower:
		return
	if event is InputEventMouseButton and !holdsPlayer and !isCreated and tower != null and money >= tower.cost:
		root.money -= tower.cost
		tower.queue_free()
		rpc("make_tower", root.active_tower_name)

# Builds the tower on all players.
remotesync func make_tower(tower):
	tower = load(tower).instance()
	tower.name = "TowerA"
	isCreated = true
	tower.get_node("StaticBody2D").layers = 1
	tower.modulate = Color(1, 1, 1, 1)
	tower.position = Vector2(48, 48)
	tower.create()
	add_child(tower)
