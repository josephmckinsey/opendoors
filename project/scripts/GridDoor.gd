extends StaticBody2D

class_name GridDoor

enum Direction {
	UP, DOWN, LEFT, RIGHT
}

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var x = 0
var y = 0
var parent
var direction: int = Direction.UP # Direction ENUM

const ENEMY = preload("res://scenes/Enemy.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	parent = get_parent()

func _input(event):
	if event is InputEventMouseButton and Input.is_mouse_button_pressed(BUTTON_LEFT):
		# if parent.get_class() == "GridWorld":
		parent.create_room(self)
		# else: 
			#print("Clicked")

"""
func spawn():
	var r = rand_range(3, 7)
	var enemy = ENEMY.instance()
	if direction == Direction.LEFT:
		enemy.position = parent.gridToMyLocal(0, r)
	elif direction == Direction.RIGHT:
		enemy.position = parent.gridToMyLocal(9, r)
	elif direction == Direction.UP:
		enemy.position = parent.gridToMyLocal(r, 0)
	elif direction == Direction.DOWN:
		enemy.position = parent.gridToMyLocal(r, 9)
	enemy.position += parent.position
	
	get_node("/root/Main").add_child(enemy)
"""

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
