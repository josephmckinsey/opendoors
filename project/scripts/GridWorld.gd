extends Node

var ROOM_WIDTH: float = Room.ROOM_WIDTH
var ROOM_HEIGHT: float = Room.ROOM_HEIGHT

var pathing: AStar2DR = AStar2DR.new()
signal updatePath

var rooms: Dictionary = {}

# Called when the node enters the scene tree for the first time.
func _ready():
	create_room_at(0, 0)
	
	# Create starting room at 0,0 "create_room_at(0, 0)"
	# Create prize
	
	
	pass

func create_room(door):
	print("Door: ", door.get_instance_id(), " ")
	match door.direction:
		GridDoor.Direction.UP:
			pass
		GridDoor.Direction.DOWN:
			pass
		GridDoor.Direction.LEFT:
			pass
		GridDoor.Direction.RIGHT:
			pass
	pass

func room_index(x: float, y: float) -> Array:
	var x_idx = int((x + ROOM_WIDTH / 2) / ROOM_WIDTH)
	var y_idx = int((y + ROOM_HEIGHT / 2) / ROOM_HEIGHT)
	return [x_idx, y_idx]
	
func get_cell(x: float, y: float) -> GridCell:
	return null
	
func get_room(x: float, y: float):
	return rooms[room_index(x, y)]
	
func create_room_at(x: float, y: float):
	pass

func disconnect_path_at(x: float, y: float):
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
