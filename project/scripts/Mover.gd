extends KinematicBody2D


const Upgrade = preload("res://scenes/Upgrades.tscn")

export  (int) var speed = 400
export (NodePath) var money_display_path = null

var velocity = Vector2(0, 0)
var ideal = Vector2(0, 0)
var ourDoor = null
const SELECTED = Color(0.5, 0.5, 0.1, 1.0)

onready var money_display: Label = get_node(money_display_path)

onready var root = get_node("/root/MainLoc").main

func get_input():
	# Movement
	if Input.is_action_pressed("left"):
		ideal.x = -1
	elif Input.is_action_pressed("right"):
		ideal.x = 1
	else:
		ideal.x = 0
	if Input.is_action_pressed("up"):
		ideal.y = -1
	elif Input.is_action_pressed("down"):
		ideal.y = 1
	else:
		ideal.y = 0
	ideal = ideal.normalized() * speed
	velocity.x += (ideal.x - velocity.x) / 3
	velocity.y += (ideal.y - velocity.y) / 3
	
	$Sprite.rotation = get_global_mouse_position().angle_to_point(position) + PI / 2
	
	# Interactions
	if Input.is_action_just_pressed("player_interact") and ourDoor != null:
		ourDoor.modulate = SELECTED
		root.rpc("voteDoor", ourDoor.x, ourDoor.y, ourDoor.name, 0)

	# if Input.is_action_just_pressed("next_round"):
	# 	root._on_door_timer()

	rpc("place", position, $Sprite.rotation_degrees)

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()

# TODO We should totally be using delta....
func _physics_process(_delta):
	if not root.dontMove:
		get_input()
	move_and_slide(velocity)

func selectDoor(door):
	ourDoor = door
	if ourDoor.modulate != SELECTED:
		ourDoor.modulate = Color(0.7, 1.0, 0.7, 1.0)

func leaveDoor(_door):
	if ourDoor.modulate != SELECTED:
		ourDoor.modulate = Color(1.0, 1.0, 1.0, 1.0)
	ourDoor = null

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	money_display.text = "$" + str(root.money)


# Here begins the class upgrade code. It's nearly identical to BaseTower.gd so
# check that out too.
func doUpgrade(attack, cost, params):
	if root.money >= cost:
		root.money -= cost
		rpc("applyChange", attack, params)
		get_parent().get_node("Upgrades").queue_free()

remotesync func applyChange(attack, params):
	attack = load(attack).instance()
	attack.setup(params)
	$Sprite.get_child(1).queue_free()
	$Sprite.add_child(attack)

func upgradeUI(event):
	if event is InputEventMouseButton and not get_parent().has_node("Upgrades"):
		# This code is broken right now.
		return
		# TODO get_child is fragile, be careful if you add things to Sprite.
		var attack = $Sprite.get_child(1)

		var upgradeUI = Upgrade.instance()
		upgradeUI.caller = self
		get_parent().add_child(upgradeUI)
		var upgrades = attack.getUpgrades()
		var downgrade = attack.getDowngrades()
		if downgrade != null:
			var downgradeButton = Button.new()
			downgradeButton.text = downgrade[1]
			downgradeButton.connect("pressed", self, "doUpgrade", [downgrade[0], downgrade[2], downgrade[3]])
			upgradeUI.addDowngrade(downgradeButton)
		for upgrade in upgrades:
			var upgradeButton = Button.new()
			upgradeButton.text = upgrade[1]
			upgradeButton.connect("pressed", self, "doUpgrade", [upgrade[0], upgrade[2], upgrade[3]])
			upgradeUI.addUpgrade(upgradeButton)
