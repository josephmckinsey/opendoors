extends StaticBody2D

signal dead
var health_bar

onready var root = get_node("/root/MainLoc").main

func _ready():
	print("Prize at", position)
	health_bar = get_node("Health/TextureProgress")

func damage(amount):
	print("Got hit")
	health_bar.set_value(health_bar.get_value() - amount)
	if health_bar.get_value() == health_bar.get_min():
		root.lost_game()
		# emit_signal("dead")
		if root.autosave:
			get_tree().quit()
