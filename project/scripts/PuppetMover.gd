extends KinematicBody2D


remote func place(pos, rot):
	position = pos
	$Sprite.rotation_degrees = rot

remotesync func applyChange(attack, params):
	attack = load(attack).instance()
	attack.set_process(false)
	attack.setup(params)
	$Sprite.get_child(0).queue_free()
	$Sprite.add_child(attack)
