extends StaticBody2D

class_name Room

const WIDTH = 1000
const HEIGHT = 1000

const ROOM_WIDTH: float = 1_000.0
const ROOM_HEIGHT: float = 1_000.0

const GRID_SIZE = 10
var grid: Dictionary = {}
var root: Node

const GRID = preload("res://scenes/GridCell.tscn")

var x = 0
var y = 0


func init():
	root = get_node("/root/MainLoc").main
	makeGrids()
	#if $down != null:
	#	$down.direction = 1
	#$left.direction = 2
	#$right.direction = 3

func makeGrids():
	for row in range(0, 10):
		for col in range(0, 10):
			var cell = $gridbox.get_child(row * 10 + col)
			if cell.canHoldTower:
				root.addTower(col, row, x, y)

func setLocation(newX, newY):
	x = newX
	y = newY
	$up.x = x
	$up.y = y
	$down.x = x
	$down.y = y
	$right.x = x
	$right.y = y
	$left.x = x
	$left.y = y

func addTower(gx, gy, tower: Node2D):
	grid[[gx, gy]] = tower
	tower.position = gridToMyLocal(gx, gy)
	add_child(tower)

func gridToMyLocal(gx, gy):
	var tileWidth = float(WIDTH-40)/GRID_SIZE
	var tileHeight = float(HEIGHT-40)/GRID_SIZE
	var xLoc = tileWidth * gx - WIDTH / 2.0 + tileWidth
	var yLoc = tileHeight * gy + tileHeight - HEIGHT / 2.0 + 20
	return Vector2(xLoc, yLoc)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
