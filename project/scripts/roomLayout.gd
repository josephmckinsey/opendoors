extends Node2D

# signal updatePaths

const GLUE_S = "res://towers/gluetrap/GlueTrap.tscn"
const AOE_S = "res://towers/aoe/Aoe.tscn"
const ARROW_S = "res://towers/arrow/Arrow.tscn"
const AOE = preload(AOE_S)
const GLUE = preload(GLUE_S)
const ARROW = preload(ARROW_S)
const ENEMIES = preload("res://EnemyResource.tres")

# Add enemy here!
const ENEMY = preload("res://enemies/ghost/Ghost.tscn")
# const FLY = preload("res://enemies/fly/Fly.tscn")

const InitialRoom = preload("res://rooms/RoomStart.tscn")
const SimpleRoom = preload("res://rooms/SimpleRoom.tscn")
const HardRoom = preload("res://rooms/HardRoom.tscn")
const ROOMS = [SimpleRoom, HardRoom]
var setRooms = [1, 0]
var room_timer = 0
var room_timer_max = 60

const AStar2DR = preload("res://scripts/AStar2DR.gd")
var yaml = preload("res://addons/godot-yaml/gdyaml.gdns").new()

onready var progress = get_node("CanvasLayer/DoorTimer/ProgressBar")

var listOfRooms:  = {}
var neighborRooms:  = {[0,0]: true}
var nextId = 0
var numPlayers = 1
var money = 0
var isServer = false
var ghostNum = 0
var myid = 0
var dontMove = false
var lost = false
var autosave = true
var roundNumber = 0

var pathing: AStar2DR = AStar2DR.new()

var directions = { "right": [1, 0]
	, "left" : [-1, 0]
	, "up" : [0, 1]
	, "down" : [0, -1]
	}

var opposites = { "right": "left"
	, "left" : "right"
	, "up" : "down"
	, "down" : "up"
	}

var current_rates = {}
var current_burst = {}

var current_votes = {}
var rounds = []
var atIndex = 0
var isPaused = true

func serialize(name):
	var data = {}
	data.listOfRooms = []
	for loc in listOfRooms:
		var room = listOfRooms[loc]
		var sRoom = {}
		sRoom.grid = []
		sRoom.path = room[0].get_filename()
		var towers = []
		for cell in room[0].get_node("gridbox").get_children():
			if cell.has_node("TowerA"):
				towers.append(cell.get_node("TowerA").serialize())
			elif cell.has_node("TowerB"):
				towers.append(cell.get_node("TowerB").serialize())
			else:
				towers.append({})
		sRoom.towers = towers

		data.listOfRooms.append([room[1], sRoom, loc])
	data.listOfRooms.sort()
	data.listOfRooms.invert()

	var save_game = File.new()
	save_game.open("user://" + name + ".save", File.WRITE)
	save_game.store_line(to_json(data))

func load(name):
	for loc in listOfRooms:
		listOfRooms[loc][0].queue_free()
	listOfRooms.clear()
	var save_game = File.new()
	save_game.open("user://" + name + ".save", File.READ)
	var data = parse_json(save_game.get_line())
	neighborRooms = {[0,0]: true}
	pathing = AStar2DR.new()

	for roomData in data.listOfRooms:
		var loc = roomData[2]
		loc = [int(loc[0]), int(loc[1])]
		var room = roomData[1]
		var roomObj = load(room.path)
		addRoomAt(loc[0], loc[1], roomObj)
		for i in range(0, len(room.towers)):
			var tower = room.towers[i]
			if "params" in tower:
				var cell = listOfRooms[loc][0].get_node("gridbox").get_child(i)
				var params = tower.params
				var towerObj = load(tower.filename).instance()
				towerObj.create(params)
				towerObj.get_node("StaticBody2D").layers = 1
				towerObj.position = Vector2(48, 48)
				towerObj.name = "TowerA"
				cell.add_child(towerObj)

	save_game.close()


func _ready():
	progress.max_value = room_timer_max
	for enemy in ENEMIES.contents:
		current_rates[enemy] = 0
		current_burst[enemy] = 0
	pause()
	addRoomAt(0, 0, InitialRoom)
	# $CanvasLayer/Notifications.root = self

func fileSort(a: String, b: String):
	return int(a.split(".")[0]) < int(b.split(".")[0])

# Only called on the host
func setup():
	var f = File.new()
	f.open("res://rounds.yaml", File.READ)
	var contents = f.get_as_text()
	var results = yaml.parse(contents)
	for result in results:
		rounds.append(result)

func start():
	isPaused = false
func pause():
	isPaused = true

func lost_game():
	lost = true

# Given a new room "node" at locations "x" and "y" and a "direction",
func cleanRooms(x: int, y: int, direction: String, node: Node2D):
# relative to the new node, remove any doors that don't make sense.
	# Holds the d's (dx and dy).
	var ds: Array = directions[direction]

	# The index of the neighbor in the list of rooms.
	var newIndex: Array = [x + ds[0], y + ds[1]]

	if listOfRooms.has(newIndex):
		# Remove the door from the new node
		node.remove_child(node.get_node(direction))

		# Remove the door from the neighbor
		var oldNode: Node2D = listOfRooms[newIndex][0]
		var opposite: String = opposites[direction]
		oldNode.remove_child(oldNode.get_node(opposite))
		var oldPath = listOfRooms[newIndex][1]
		var newPath = nextId
		for i in range (3, 7):
			if opposite == "left":
				pathing.connect_points(newPath*100+90+i, oldPath*100+i)
			elif opposite == "right":
				pathing.connect_points(oldPath*100+90+i, newPath*100+i)
			elif opposite == "down":
				pathing.connect_points(oldPath*100+10*i+9, newPath*100+10*i)
			elif opposite == "up":
				pathing.connect_points(newPath*100+10*i+9, oldPath*100+10*i)

# Given "x" and "y" coordinates, add a new Room instance and fix any doors.
remote func addRoomAt(x, y, Room=null):
	assert(!listOfRooms.has([x, y]))
	assert(neighborRooms.erase([x, y]))
	if Room == null:
		var r = randf()
		for i in range(ROOMS.size()):
			if r < setRooms[i]:
				Room = ROOMS[i]
				break
	# If it is still null...
	if Room == null:
		Room = ROOMS[0]
	for dir in directions:
		var newDir = [x + directions[dir][0], y + directions[dir][1]]
		if not (newDir in listOfRooms):
			neighborRooms[newDir] = true

	for i in range(0, 10):
		for j in range(0, 10):
			var id = nextId*100 + i*10 + j
			pathing.add_point2d(id, Vector2(x*1000+i*96-480+48, y*-1000+j*96-480+48))


	for i in range(0, 10):
		for j in range(0, 10):
			var right = nextId*100 + i*10 + j + 1
			var down = nextId*100 + i*10 + j + 10
			var id = nextId*100 + i*10 + j
			if j < 9:
				pathing.connect_points(id, right)
			if i < 9:
				pathing.connect_points(id, down)
#	pathing.connect_points(nextId*100+99, nextId*100+98)
#	pathing.connect_points(nextId*100+89, nextId*100+89)
	var node: Node2D = Room.instance()
	node.setLocation(x, y)
	# Hard coded to Room size.
	node.position = Vector2(x*node.WIDTH, y*-node.HEIGHT)

	for dir in directions.keys():
		cleanRooms(x, y, dir, node)

	$Rooms.add_child(node)
	listOfRooms[[x, y]] = [node, nextId]
	node.init()
	nextId += 1

remotesync func addTower(x, y, roomx, roomy):
	var roomNum = listOfRooms[[roomx, roomy]][1]
	var id = roomNum * 100 + x*10 + y
	var right = roomNum*100 + x*10 + y + 1
	var down = roomNum*100 + x*10 + y + 10
	var up = roomNum*100 + x*10 + y - 10
	var left = roomNum*100 + x*10 + y - 1

	pathing.disconnect_points(id, right)
	pathing.disconnect_points(id, up)
	pathing.disconnect_points(id, down)
	pathing.disconnect_points(id, left)
	# emit_signal("updatePaths")


remote func fromDoor(x, y, direction):
	assert(listOfRooms.has([x, y]))
	assert(!listOfRooms.has([x, y] + directions[direction]))

	var ds = directions[direction]
	var newRoom = [x + ds[0], y + ds[1]]
	addRoomAt(newRoom[0], newRoom[1])

master func voteDoor(x, y, direction, player):
	current_votes[player] = [x, y, direction]


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Prize_dead():
	get_tree().quit()

var active_tower = ARROW
var active_tower_name = ARROW_S
func _set_active_tower_wall():
	active_tower_name = GLUE_S
	active_tower = GLUE
func _set_active_tower_aoe():
	active_tower_name = AOE_S
	active_tower = AOE
func _set_active_tower_arrow():
	active_tower_name = ARROW_S
	active_tower = ARROW

# Handles moving the game forward based on the timer.dat
func _process(delta):
	if not isPaused:
		room_timer += delta
	var currentTime = room_timer
	if room_timer >= room_timer_max:
		on_door_timer(false)
	progress.value = currentTime
	if isServer:
		if rounds.size() > 0 and atIndex < rounds[0].size():
			var r = rounds[0][atIndex]
			if currentTime >= r.time:
				if r.event == "spawn":
					current_rates[r.enemy] = r.rate
					current_burst[r.enemy] = r.burst
				elif r.event == "message":
					rpc("send_message", r.message)
				elif r.event == "roomOdds":
					if r.room == "Simple":
						setRooms[0] = r.rate
					elif r.room == "Hard":
						setRooms[1] = r.rate
				atIndex += 1


# Called every time a door needs to be opened.
# Open one of the doors opened by a player.
# If a player doesn't vote, they get assigned a random door.
func on_door_timer(fromSkip=true):
	if isServer:
		rounds.pop_front()

		var chose = randi() % numPlayers
		if chose < current_votes.keys().size():
			chose = current_votes.keys()[chose]
			var vote = current_votes[chose]
			rpc("fromDoor", vote[0], vote[1], vote[2])
			fromDoor(vote[0], vote[1], vote[2])
		else:
			var vote = randi() % neighborRooms.keys().size()
			var newRoom = neighborRooms.keys()[vote]
			rpc("addRoomAt", newRoom[0], newRoom[1])
			addRoomAt(newRoom[0], newRoom[1])
		for voter in current_votes:
			var door_loc = current_votes[voter]
			var door = listOfRooms[[door_loc[0], door_loc[1]]][0].get_node(door_loc[2])
			if door != null:
				door.modulate = Color(1.0, 1.0, 1.0, 1.0)
		current_votes.clear()

		for enemy in ENEMIES.contents:
			current_rates[enemy] = 0
			current_burst[enemy] = 0
		setRooms = [0, 0]
		atIndex = 0
		room_timer -= room_timer_max
		room_timer = max(room_timer, 0)
		if autosave and not fromSkip:
			serialize("autosave-" + str(roundNumber))
		roundNumber += 1


remote func create_ghost(i, pos, n):
	var enemy = ENEMY.instance()
	enemy.enemyType = i
	enemy.position = pos
	enemy.name = "Ghost" + str(n)

	add_child(enemy)

remotesync func send_message(m):
	var notif = get_node("CanvasLayer/Notifications")
	notif.add_message(m)
