extends "res://addons/gut/test.gd"

func setup_main(speed):
	Engine.time_scale = speed
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(5656, 1)
	get_tree().network_peer = peer
	var MainLoc = load("res://scenes/MainLoc.tscn")
	var Main = load("res://scenes/Main.tscn")
	var main = Main.instance()
	var main_loc = MainLoc.instance()
	main_loc.main = main
	main_loc.name = "MainLoc"
	if get_node("/root/MainLoc"):
		get_node("/root/MainLoc").main = main
	else:
		get_node("/root/").add_child(main_loc)
	add_child(main)

	main.isServer = true
	main.setup()
	main.numPlayers = 1
	main.myid = str(1)
	main.autosave = false
	return main

func teardown(main):
	get_node("/root/MainLoc").free()
	main.free()
	Engine.time_scale = 1


# func test_can_lose_1():
# 	var main = setup_main(10)
# 	main.start()
# 	yield(yield_for(30), YIELD)
# 	assert_true(main.lost, "You lose the game if you do nothing")
# 	teardown(main)

# func test_round_6():
# 	var main = setup_main(1)
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.load("full_fast_round_6")
# 	main.start()
# 	yield(yield_for(60), YIELD)
# 	assert_true(not main.lost, "You can win the first round by placing strong towers")
# 	teardown(main)

# func test_round_7():
# 	var main = setup_main(1)
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.load("full_fast_round_7_lose")
# 	main.start()
# 	yield(yield_for(60), YIELD)
# 	assert_true(main.lost, "You can can't win with only fast towers")
# 	teardown(main)

# func test_round_5():
# 	var main = setup_main(1)
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.load("full_damage_round_5")
# 	main.start()
# 	yield(yield_for(60), YIELD)
# 	assert_true(not main.lost, "You can win with only strong towers")
# 	teardown(main)

# func test_round_6():
# 	var main = setup_main(1)
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.load("full_damage_round_6")
# 	main.start()
# 	yield(yield_for(60), YIELD)
# 	assert_true(main.lost, "You can't win with only strong towers")
# 	teardown(main)

# func test_round_7():
# 	var main = setup_main(2)
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.on_door_timer()
# 	main.load("full_damage_round_7")
# 	main.start()
# 	yield(yield_for(180), YIELD)
# 	assert_true(not main.lost, "You can win with only strong towers")
# 	teardown(main)

# This setup fails if you add in another round, but I think that's fine since
# the layout is pretty tough.
func test_round_9():
	var main = setup_main(2)
	main.on_door_timer()
	main.on_door_timer()
	main.on_door_timer()
	main.on_door_timer()
	main.on_door_timer()
	main.on_door_timer()
	main.on_door_timer()
	main.on_door_timer()
	main.load("full_mixed_round_8")
	main.start()
	yield(yield_for(60), YIELD)
	assert_true(not main.lost, "You can win with a mix")
	teardown(main)
