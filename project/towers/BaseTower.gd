extends Node2D

# This file defines the basic behavior ever tower needs to implement. This
# includes making sure the tower doesn't activate too early and handling
# upgrades.

const Upgrade = preload("res://scenes/Upgrades.tscn")
onready var root = get_node("/root/MainLoc").main
onready var canvas = root.get_node("CanvasLayer")
var yaml = preload("res://addons/godot-yaml/gdyaml.gdns").new()

var created = false
var ogMod = modulate

var upgrades = {}
var upgradeState = {}
var atts = {}
var lastParams = {}

func create(params={}):
	lastParams = params
	loadUpgrades()
	for key in params:
		upgradeState[key] = params[key]
	for stat in upgrades.default:
		atts[stat] = upgrades.default[stat]
	for upgradeType in upgrades:
		if upgradeType == "default":
			continue
		var i = upgradeState[upgradeType] - 1
		if i > -1:
			for stat in upgrades[upgradeType][i]:
				if not (stat in ["cost", "name", "before", "after"]):
					atts[stat] = upgrades[upgradeType][i][stat]
	setup()

# Exists to make the syntax checker happy. Think of it as an abstract function
func get_name():
	pass
func get_SELF():
	pass
func run(_delta):
	pass
func setup():
	pass

func loadUpgrades():
	if upgrades.empty():
		var file = File.new()
		file.open("res://towers.yaml", File.READ)
		file = file.get_as_text()
		# print(file)
		# print(JSON.parse(file).error_line)
		# print(JSON.parse(file).error)
		upgrades = yaml.parse(file)[get_name()]
		for upgradeType in upgrades:
			if upgradeType == "default":
				continue
			upgradeState[upgradeType] = 0
		# print(upgrades)
		# print(upgradeState)

func validUpgrades():
	var allUpgrades = []
	for upgradeType in upgrades:
		if upgradeType == "default":
			continue
		if upgradeState[upgradeType] < upgrades[upgradeType].size():
			var isValid = true
			var maybeUpgrade = upgrades[upgradeType][upgradeState[upgradeType]]

			if "after" in maybeUpgrade:
				for a in maybeUpgrade.after:
					if maybeUpgrade.after[a] > upgradeState[a]:
						isValid = false

			if "before" in maybeUpgrade:
				for a in maybeUpgrade.before:
					if maybeUpgrade.before[a] <= upgradeState[a]:
						isValid = false

			if isValid:
				var newUpgradeState = upgradeState.duplicate()
				newUpgradeState[upgradeType] += 1
				var towerType = get_SELF()
				if "newTower" in maybeUpgrade:
					towerType = maybeUpgrade.newTower
				allUpgrades.append([towerType, maybeUpgrade.name + " - $" + str(maybeUpgrade.cost), maybeUpgrade.cost, newUpgradeState])
	return allUpgrades

# Called when the user clicks an upgrade or downgrade button
func applyChangeLocal(tower, cost, params):
	var money = root.money
	if money >= cost:
		root.money -= cost
		canvas.get_node("Upgrades").queue_free()
		rpc("applyChange", tower, params)


# Called on all players to create an updated tower
remotesync func applyChange(tower, params):
	tower = load(tower).instance()
	tower.create(params)

	tower.get_node("StaticBody2D").layers = 1
	tower.position = Vector2(48, 48)
	if name == "TowerA":
		tower.name = "TowerB"
	else:
		tower.name = "TowerA"
	get_parent().add_child(tower)
	queue_free()

func _physics_process(delta):
	# Layer 0 means the tower is in ghost mode
	if get_node("StaticBody2D").layers == 0:
		return
	# The first time the tower isn't in ghost mode, create the upgrade
	# capabilities
	if not created:
		var c = Control.new()
		c.connect("gui_input", self, "onMouseClick")
		c.margin_left = -48
		c.margin_top = -48
		c.margin_right = 48
		c.margin_bottom = 48
		add_child(c)
		created = true

	run(delta)

# Abstract function. See Ranged.gd for a description of the format.
func getUpgrades():
	return validUpgrades()

# Abstract function. See Ranged.gd for a description of the format.
func getDowngrades():
	return null

# When you click the tower, display the upgrade UI
func onMouseClick(event):
	if event is InputEventMouseButton and not canvas.has_node("Upgrades"):
		# Arbitrarily chosen color to signify a tower is selected
		self.modulate = Color(0.5, 0.5, 1.0, 1.0)

		var upgradeUI = Upgrade.instance()
		upgradeUI.caller = self
		canvas.add_child(upgradeUI)
		var downgrade = getDowngrades()
		if downgrade != null:
			var downgradeButton = Button.new()
			downgradeButton.text = downgrade[1]
			# If the tower doesn't tell us the upgrade cost, try to find it
			if downgrade.size() == 2:
				var tmpTower = load(downgrade[0]).instance()
				downgrade.append(tmpTower.cost)
				downgrade.append(null)
				tmpTower.queue_free()
			downgradeButton.connect("pressed", self, "applyChangeLocal", [downgrade[0], downgrade[2], downgrade[3]])
			upgradeUI.addDowngrade(downgradeButton)

		# Basically the same as downgrade but in a for instead of an if
		var upgrades = getUpgrades()
		for upgrade in upgrades:
			var upgradeButton = Button.new()
			upgradeButton.text = upgrade[1]
			if upgrade.size() == 2:
				var tmpTower = load(upgrade[0]).instance()
				upgrade.append(tmpTower.cost)
				upgrade.append(null)
				tmpTower.queue_free()
			upgradeButton.connect("pressed", self, "applyChangeLocal", [upgrade[0], upgrade[2], upgrade[3]])
			upgradeUI.addUpgrade(upgradeButton)

func serialize():
	return {
			"filename": get_filename(),
			"params": lastParams,
	}
