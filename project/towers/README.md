# How to add a tower

1. Create a new folder and main tower gd and tscn file. The gd file must extend
   the BaseTower.gd file. See the other towers for an example.

2. Add a new button to the Main.tscn file and a new function in the
   RoomLayout.gd file. See the buttons/functions for the existing towers for an
   example.

That should be it! If it's not, let me (jhgarner) know so I can update the
README or fix the code.
