extends Control

export var downgradeParentPath: NodePath
export var upgradeParentPath: NodePath

onready var downgradeParent = get_node(downgradeParentPath)
onready var upgradeParent = get_node(upgradeParentPath)

var caller: Node

func addDowngrade(button):
	downgradeParent.add_child(button)

func addUpgrade(button):
	upgradeParent.add_child(button)

func closeUI():
	caller.modulate = caller.ogMod
	queue_free()
