extends "res://towers/BaseTower.gd"

# All nodes that will be attacked
var inRange = {}

const rate = 1
const cost = 6

onready var Wall = "res://towers/wall/Wall.tscn"

func onEnter(node):
	inRange[node] = true

func onExit(node):
	inRange.erase(node)

	
# Called every frame by the BaseTower
func run(delta):
	for node in inRange:
		node.damage(rate * delta)


func getUpgrades():
	return [[Wall, "Wall"]]
func getDowngrades():
	return null
