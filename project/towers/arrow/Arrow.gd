extends "res://towers/BaseTower.gd"

const PROJ = preload("res://towers/arrow/Projectile.tscn")
onready var SELF = "res://towers/arrow/Arrow.tscn"
onready var FLAMETHROWER = "res://towers/flamethrower/FlameThrower.tscn"
onready var BOMBER = "res://towers/bomber/Bomber.tscn"

# Stock DPS = .5
# rate1 = .8
# rate2 = 1.143
# rate3 = 1.4545
# rate4 = 1.684
# damage1 = 1.125
# damage2 = 1.594
# damage3 = 1.945
# damage4 = 2.209

# Damage is pretty noticeably better than speed at early stages looking entirely at DPS.
# Against a large stream of enemies though it becomes worse. How do I quantify that?


var inRange = {}
const cost = 4

func get_SELF():
	return SELF
func get_name():
	return "Arrow"

func onEnter(node):
	inRange[node] = true

func onExit(node):
	inRange.erase(node)

func setup():
	modulate = Color(pow(0.5, upgradeState.Speed), pow(0.75, upgradeState.Damage), 1.0, 1.0)
	$Area2D/CollisionShape2D.shape.radius = atts.radius
	ogMod = modulate
	$Timer.wait_time = atts.rate

func _fire():
	if inRange.size() > 0 && get_node("StaticBody2D").layers != 0:
		var proj = PROJ.instance()
		proj.position = global_position
		proj.damage = atts.damage
		var target = inRange.keys()[0]

		proj.velocity = (target.position  - proj.position).normalized() * 0.5
		root.add_child(proj)
