extends Node2D

var velocity: Vector2
var damage = 0


func _ready():
	$Sprite.rotation = get_angle_to(position + velocity * 100) + PI/2

func _on_impact(node):
	node.damage(damage)
	queue_free()

func _physics_process(delta):
	position += velocity * delta * 2000

func _exit_range():
	queue_free()
