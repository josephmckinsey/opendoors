extends "res://towers/BaseTower.gd"

const PROJ = preload("res://towers/bomber/Projectile.tscn")
onready var SELF = "res://towers/bomber/Bomber.tscn"

# onready var FLAMETHROWER = "res://towers/flamethrower/FlameThrower.tscn"

# Stock DPS = 3.125
# damage1 = 3.125 (Must be that one)
# damage2 = 3.594
# damage3 = 3.945
# damage4 = 4.209

# Damage is pretty noticeably better than speed at early stages looking entirely at DPS.
# Against a large stream of enemies though it becomes worse. How do I quantify that?


var inRange = {}

func get_SELF():
	return SELF
func get_name():
	return "Bomber"

func onEnter(node):
	inRange[node] = true

func onExit(node):
	inRange.erase(node)

func setup():
	modulate = Color(pow(0.5, upgradeState.Speed), pow(0.75, upgradeState.Damage), 1.0, 1.0)
	ogMod = modulate
	$Timer.wait_time = atts.rate

func _fire():
	if inRange.size() > 0 && get_node("StaticBody2D").layers != 0:
		var proj = PROJ.instance()
		proj.position = global_position
		proj.damage = atts.damage
		var target = inRange.keys()[0]

		proj.velocity = (target.position  - proj.position).normalized() * 1
		root.add_child(proj)
