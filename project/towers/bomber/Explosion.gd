extends Node2D

var velocity: Vector2
var damage = 0

func _on_impact(node):
	node.damage(damage)

func _physics_process(delta):
	position += velocity * delta * 2000

func _exit_range():
	queue_free()
