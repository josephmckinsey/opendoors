extends Node2D

var velocity: Vector2
var damage = 0

const EXPLOSION = preload("res://towers/bomber/Explosion.tscn")

func _on_impact(_node):
	var explosion = EXPLOSION.instance()
	explosion.damage = damage
	get_parent().call_deferred("add_child", explosion)
	explosion.global_position = global_position
	queue_free()

func _physics_process(delta):
	position += velocity * delta * 1000

func _exit_range():
	queue_free()
