extends "res://towers/BaseTower.gd"

const PROJ = preload("res://towers/flamethrower/Projectile.tscn")
onready var SELF = "res://towers/flamethrower/FlameThrower.tscn"


# Stock DPS = 2.5
# rate1 = 2.5 (You can't get rate 0)
# rate2 = 3.333
# rate3 = 4
# rate4 = 4.444

var inRange = {}
var hit = {}

func get_SELF():
	return SELF
func get_name():
	return "Flames"

func onEnter(node):
	inRange[node] = true

func onExit(node):
	inRange.erase(node)

func onEnterDanger(node):
	hit[node] = true

func onExitDanger(node):
	hit.erase(node)

func setup():
	modulate = Color(pow(0.5, upgradeState.Speed), pow(0.75, upgradeState.Damage), 1.0, 1.0)
	ogMod = modulate
	$Timer.wait_time = atts.rate

func run(delta):
	if inRange.size() > 0 && get_node("StaticBody2D").layers != 0:
		var proj = PROJ.instance()
		proj.position = global_position
		proj.damage = atts.damage
		var target = inRange.keys()[0]
		$Sprite.rotation = get_angle_to(target.position) + PI / 2
		$Sprite.get_node("Sprite2").show()
	else:
		$Sprite.get_node("Sprite2").hide()

	for node in hit:
		node.damage(atts.damage * delta)

		# proj.velocity = (target.position  - proj.position).normalized() * 1
		# root.add_child(proj)
