extends Node2D

var velocity: Vector2
var damage = 0

func _on_impact(node):
	node.damage(damage)
	queue_free()

func _physics_process(delta):
	$Sprite.rotation = get_angle_to(position + velocity)
	position += velocity * delta * 1

func _exit_range():
	queue_free()
