extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var parent = get_parent()
onready var oldSpeed = 0
var amount = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	parent.speedMod *= amount

func destroy():
	parent.speedMod /= amount
	queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
