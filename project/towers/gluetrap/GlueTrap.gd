extends "res://towers/BaseTower.gd"

const PROJ = preload("res://towers/gluetrap/Projectile.tscn")
onready var SELF = "res://towers/gluetrap/GlueTrap.tscn"

var inRange = {}
var rate = 1
var rateUpgrade = 0
const cost = 4
var power = 0.5
var powerUpgrade = 0
var glueRange = 48
var glueRangeUpgrade = 0

func onEnter(node):
	inRange[node] = true

func onExit(node):
	inRange.erase(node)

# func setup():
# 	rateUpgrade = ps[0]
# 	powerUpgrade = ps[1]
# 	glueRangeUpgrade = ps[2]
# 	rate = 0.5 + 0.25 * pow(0.5, ps[0])
# 	power = 0.75 + 0.1 * pow(0.5, ps[1])
# 	glueRange = 48 + 48 * ps[2]
# 	modulate = Color(pow(0.5, ps[0]), pow(0.75, ps[1]), 1.0, 1.0)
# 	ogMod = modulate
# 	$Timer.wait_time = rate

func getUpgrades():
	var faster = [[SELF, "Faster Placement - $2", 2, [rateUpgrade+1, powerUpgrade, glueRangeUpgrade]]]
	var stronger = [[SELF, "Stronger Effect - $2", 2, [rateUpgrade, powerUpgrade+1, glueRangeUpgrade]]]
	var longer = [[SELF, "Larger Range - $2", 2, [rateUpgrade, powerUpgrade, glueRangeUpgrade+1]]]
	var allUpgrades = []
	# var flames = [[FLAMETHROWER, "Flamethrower - $4", 4, [rateUpgrade, powerUpgrade, glueRangeUpgrade]]]
	if rateUpgrade >= 1:
		allUpgrades += faster
	elif powerUpgrade >= 1:
		allUpgrades += stronger
	else:
		allUpgrades += faster + stronger
	if glueRangeUpgrade < 3:
		allUpgrades += longer
	return allUpgrades

func _fire():
	if get_node("StaticBody2D").layers != 0:
		var proj = PROJ.instance()
		var locx = ((randi() % 2) * 2 - 1) * (randf() * glueRange + 60)
		var locy = ((randi() % 2) * 2 - 1) * (randf() * glueRange + 60)
		proj.position = global_position
		proj.position.x += locx
		proj.position.y += locy
		proj.amount = power
		proj.init(glueRangeUpgrade * 3 + 3)
		get_node("/root/Main").add_child(proj)
