extends Node2D

var velocity: Vector2
var amount = 0
var health = 1
const EFFECT = preload("res://towers/gluetrap/Effect.tscn")

func init(length):
	$Timer.wait_time = length
	print("Init")
	print($Timer.wait_time)
	print(length)

func _ready():
	print("ready")
	$Timer.stop()
	print($Timer.wait_time)
	$Timer.start()

func _on_impact(node):
	var effect = EFFECT.instance()
	effect.amount = amount
	node.add_child(effect)
	health -= 1
	if health == 0:
		queue_free()

func _exit_range():
	queue_free()
