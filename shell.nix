{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    (pkgs.python38.withPackages (ps: with ps; [ requests jedi pylint pynvim autopep8 pyyaml jupyterlab python-language-server ]))
    pkgs.godot-server
    pkgs.godot-headless

    # keep this line if you use bash
    pkgs.bashInteractive
  ];
}
